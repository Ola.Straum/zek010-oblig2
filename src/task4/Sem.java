package src.task4;

public class Sem {
    private int permits;

    public Sem(int permits) {

        this.permits = permits;
    }

    public synchronized void acquire() throws InterruptedException {
        if (permits > 0) {
            // TODO: Your solution goes here.
            permits--;
        } else {
            // TODO: Your solution goes here.
            this.wait();
            permits--;
        }
    }
    public synchronized void release() {
      // TODO: Your solution goes here.
      permits++;
      this.notify();
    }
    }


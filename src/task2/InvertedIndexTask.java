/**package src.task2;

import javax.swing.text.Document;
import java.util.Map;
import java.util.concurrent.Future;

public class InvertedIndexTask implements Runnable {
    @Override
public void run() {
        try {
            while (!Thread.interrupted()) {
                Document document = completionService.take().get();
                updateInvertedIndex(document.getVoc(), document.getFileName());
                }
            while (true) {
                Future<Document> future = completionService.poll();
                if (future == null) break;
                Document document = future.get();
                updateInvertedIndex(document.getVoc(), document.getFileName());
                }
            } catch (InterruptedException | ExecutionException e) {
            Thread.currentThread().interrupt();
            }}

        private void updateInvertedIndex(Map<String, Integer> voc, String fileName) {
        for (String word : voc.keySet()) {
            if (word.length() >= 3) {
                StringBuffer buffer =
                        invertedIndex.computeIfAbsent(word, k -> new StringBuffer());
                buffer.append(fileName + ";");
            }
        }
    }
}
 */


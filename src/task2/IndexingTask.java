/**package src.task2;

import javax.swing.text.Document;
import javax.swing.text.html.parser.DocumentParser;
import java.io.File;
import java.util.Map;
import java.util.concurrent.Callable;

public class IndexingTask implements Callable<Document> {
    private final File file;

    public IndexingTask(File file) {
        this.file = file;
    }

    @Override
    public Document call() {
        DocumentParser parser = new DocumentParser();
        Map<String, Integer> voc = parser.parse(file.getAbsolutePath());
        Document document = new Document();
        document.setFileName(file.getName());
        document.setVoc(voc);
        return document;
    }
}
*/
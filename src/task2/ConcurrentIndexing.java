/**package src.task2;

public class ConcurrentIndexing {
    private ConcurrentHashMap<String, StringBuffer> invertedIndex;
private ExecutorCompletionService<Document> completionService;
private int executionTime;

        public int getExecutionTime() {

        return executionTime;

}
 public void index(File[] files) {
        int numCores = Math.max(Runtime.getRuntime().availableProcessors() - 1, 1);
        ThreadPoolExecutor executor = (ThreadPoolExecutor)
        Executors.newFixedThreadPool(numCores);
        completionService = new ExecutorCompletionService<>(executor);
        invertedIndex = new ConcurrentHashMap<>();
        InvertedIndexTask invertedIndexTask = new InvertedIndexTask();
        Thread thread1 = new Thread(invertedIndexTask);
        thread1.start();

        InvertedIndexTask invertedIndexTask2 = new InvertedIndexTask();
        Thread thread2 = new Thread(invertedIndexTask2);
        thread2.start();

        for (File file : files) {
        IndexingTask task = new IndexingTask(file);
        completionService.submit(task);
        if (executor.getQueue().size() > 1000) {
        do {
        try {
        TimeUnit.MILLISECONDS.sleep(50);
        } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
        }
        } while (executor.getQueue().size() > 1000);
        }
        }
        executor.shutdown();
        try {
        executor.awaitTermination(1, TimeUnit.DAYS);
        thread1.interrupt(); thread2.interrupt();
        thread1.join(); thread2.join();
        } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
        }

        }
}*/

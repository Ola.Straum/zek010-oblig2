package src.task5;

import java.util.List;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Process implements Runnable {
    int id;
    List dolls;
    CyclicBarrier stageA, stageB, stageC;
    public Process(int id, List dolls, CyclicBarrier stageA, CyclicBarrier stageB, CyclicBarrier stageC) {
        this.id = id;
        this.stageA = stageA;
        this.stageB = stageB;
        this.stageC = stageC;
        this.dolls = dolls;
    }
    public void run() {
        // TODO: Your solution goes here

        Doll doll = assembly();
        try {
            stageA.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
        painting(doll);
        try {
            stageB.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
        qualityControl(doll);
        try {
            stageC.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
    }
void painting(Doll d) {
        d.setPainted(true);
    }
Doll assembly() {
        Random r = new Random();
        return new Doll(id, r.nextInt(4) + 7);
        }
void qualityControl(Doll d) {
        if (d.getQualityScore() >= 9) {
            d.hasImperfections(false);
            dolls.add(d);
        }
    }
}
